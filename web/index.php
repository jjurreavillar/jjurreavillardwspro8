<?php
session_start();

require_once __DIR__ . '/../app/config.php';
require_once __DIR__ . '/../app/Controller.php';
require_once __DIR__ . '/../app/Model.php';

$map = array(
    'indice' => array('controller' =>'Controller', 'action' =>'indice'),
    'cerrar' => array('controller' =>'Controller', 'action' => 'cerrar'),
    'insertarP' => array('controller' =>'Controller', 'action' =>'insertarP'),
    'insertarS' => array('controller' =>'Controller', 'action' =>'insertarS'),
    'ver' => array('controller' =>'Controller', 'action' =>'ver'),
);

if (isset($_GET['ctl']))
{
	if (isset($map[$_GET['ctl']]))
		$ruta = $_GET['ctl'];
    else
    {
        header('Status: 404 Not Found');
        echo '<html><body><p>Error 404: No existe la ruta <i>' . $_GET['ctl'] .'</p></body></html>';
        die();
        exit();
    }
}
else
    $ruta = 'indice';

$controlador = $map[$ruta];

if (method_exists($controlador['controller'],$controlador['action']))
	call_user_func(array(new $controlador['controller'], $controlador['action']));
else
{
		header('Status: 404 Not Found');
		echo '<html><body><p>Error 404: El controlador <i>' . $controlador['controller'] . '->' . $controlador['action'] . '</i> no existe</p></body></html>';
}
?>
