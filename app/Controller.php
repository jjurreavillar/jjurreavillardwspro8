<?php
class Controller
{
    public function indice()
    {
        if (isset($_POST['login']) && isset($_POST['nombre']) && isset($_POST['pass']))
        {
            $nombreEnviado = htmlspecialchars(trim(strip_tags($_POST['nombre'])));
            $passEnviado = htmlspecialchars(trim(strip_tags($_POST['pass'])));

            $m = new Model();
            $m->verificarUsuario($nombreEnviado, $passEnviado);
            unset($m);
        }
        
        if (isset($_SESSION["autorizado"]) && $_SESSION["autorizado"])
            require __DIR__ . "/templates/indice.php";
        else
            require __DIR__ . "/templates/login.php";
    }
    
    public function cerrar()
    {
        session_destroy();
        
        require __DIR__ . "/templates/login.php";
    }
    
    public function insertarP()
    {
        if (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"])
            header("Location: " . __DIR__ . "/templates/login.php");

        if (isset($_POST['insertarP']))
        {
            $id = htmlspecialchars(trim(strip_tags($_POST['id'])));
            $nombre = htmlspecialchars(trim(strip_tags($_POST['nombre'])));
            $telefonos = $_POST['telefonos'];

            $m = new Model();
            $m->insertarProgramador(new Programador($id, $nombre, $telefonos));
            unset($m);

            require __DIR__ . "/templates/indice.php";
        }
        else
            require __DIR__ . "/templates/insertarP.php";
    }
    
    public function insertarS()
    {
        if (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"])
            header("Location: " . __DIR__ . "/templates/login.php");
        
        if (isset($_POST['insertarS']))
        {
            $id = htmlspecialchars(trim(strip_tags($_POST['id'])));
            $nombre = htmlspecialchars(trim(strip_tags($_POST['nombre'])));
            $pId = htmlspecialchars(trim(strip_tags($_POST['programador'])));

            $m = new Model();
            $m->insertarSoftware(new Software($id, $nombre, $pId));
            unset($m);
            
            require __DIR__ . "/templates/indice.php";
        }
        else
        {
            $m = new Model();
            $programadores = $m->getProgramadores();
            
            require __DIR__ . "/templates/insertarS.php";
            
            unset($m);
        }
    }
    
    public function ver()
    {
        if (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"])
            header("Location: " . __DIR__ . "/templates/login.php");
        
        $m = new Model();
        $softwares = $m->getSoftware();
        $programadores = $m->getProgramadores();
        
        foreach($softwares as $s)
            if ($s->getId() == $_GET["id"])
            {
                $software = $s;
                break;
            }
        
        foreach($programadores as $p)
            if ($p->getId() == $software->getProgramador())
            {
                $programador = $p;
                break;
            }
        
        require __DIR__ . "/templates/ver.php";
        
        unset ($m);
    }
}
?>
