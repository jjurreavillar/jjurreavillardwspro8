<?php ob_start() ?>
<h2>Datos del Software</h2>
<div class="contenedor-formulario">
    <form method="POST" action="index.php?ctl=insertarS" >
        <caption>Introduzca los datos del software</caption>
        <ul class="contenedor-formulario-flex">
            <li>
                <label for="id">Identificador:</label>
                <input type="text" id="id" name="id" placeholder="639" pattern="\d+" title="Únicamente se permiten números" required />
            </li>
            <li>
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" placeholder="Facturación y tal" required />
            </li>
            <li>
                <label for="programador">Programadores:</label>
                <select id="programador" name="programador">
                    <?php
                    if (count($programadores) > 0)
                        foreach ($programadores as $programador)
                        {
                            echo "<option value=\"".$programador->getId()."\">";
                            echo $programador->getNombre()."</option>";
                        }
                    else
                        echo "<option value=\"\">Ningún programador encontrado</option>";
                    ?>
                </select>
            </li>
            <li>
                <input type="submit" value="Enviar" name="insertarS">
                <input type="reset" value="Borrar">
            </li>
        </ul>
    </form>
</div>
<?php $articulo = ob_get_clean() ?>

<?php include "layout.php" ?>
