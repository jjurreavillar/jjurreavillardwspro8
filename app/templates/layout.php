<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Proyecto</title>
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/principal.css">
    </head>
    <body>
        <header>
<?php 
if (isset($_SESSION["autorizado"]) && $_SESSION["autorizado"])
{
    echo '<label id="saludo">Hola, ' . $_SESSION["nombre"] . '. </label>';
    echo '<a id="cerrarSesion" href="index.php?ctl=cerrar">Haz clic aquí para cerrar la sesión</a>';
}
?>
            <h1><?=TITULO?></h1>
            <h3>Proyecto UD8 DWC</h3>
        </header>
        <div id="contenedor">
            <article>
<?php echo $articulo ?>
            </article>
<?php if (isset($menu)): ?>
<?=$menu?>
<?php endif; ?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>
