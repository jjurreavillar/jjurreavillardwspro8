<?php ob_start() ?>
<h2>Datos técnicos</h2>
<div>
    <p>ID interna: <?=$software->getId()?></p>
    <p>Nombre: <?=$software->getNombre()?></p>
    <p>Desarrollador asignado: <?=$programador->getNombre()?></p>
    <br>
</div>
<h2>Equipo de desarrollo</h2>
<div>
    <p>Nombre: <?=$programador->getNombre()?></p>
    <p>Identificador: <?=$programador->getId()?></p>
<?php
foreach ($programador->getTelefonos() as $indice => $tlf)
{
    echo "<p>";

    if ($indice == 0)
        echo "Teléfonos de contacto: ";

    echo $tlf."</p>";
}
echo "<br>";
?>
</div>
<?php $articulo = ob_get_clean() ?>

<?php ob_start() ?>
<?php
if (isset($softwares) && count($softwares) > 0)
{
    echo "<aside><nav><p>Software disponible:</p><ul>";
    
    foreach ($softwares as $software)
        echo '<li><a href="index.php?ctl=ver&id=' . $software->getId() . '" title="' . $software->getNombre() . '">' . $software->getNombre() . '</a></li>';
    
    echo "</ul></nav></aside>";
}
?>
<?php $menu = ob_get_clean() ?>

<?php include "layout.php" ?>
