<?php ob_start() ?>
<h2>Datos del Programador</h2>
<div class="contenedor-formulario">
    <form method="POST" action="index.php?ctl=insertarP" >
        <caption>Introduzca los datos del programador</caption>
        <ul class="contenedor-formulario-flex">
            <li>
                <label for="id">Identificador:</label>
                <input type="text" id="id" name="id" placeholder="8419" pattern="\d+" title="Únicamente se permiten números" required />
            </li>
            <li>
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" placeholder="Fulanito Pérez" pattern="[a-zA-Z\s]+" title="El nombre solo puede contener letras y espacios" required />
            </li>
            <li>
                <label for="telefono1">Teléfono:</label>
                <input type="tel" id="telefono1" name="telefonos[]" placeholder="123456789" pattern="\d{9}" title="El teléfono deben ser 9 números" required/>
            </li>
            <li>
                <input type="tel" id="telefono2" name="telefonos[]" placeholder="123456789" pattern="\d{9}" title="El teléfono deben ser 9 números"/>
            </li>
            <li>
                <input type="submit" value="Enviar" name="insertarP">
                <input type="reset" value="Borrar">
            </li>
        </ul>
    </form>
</div>
<?php $articulo = ob_get_clean() ?>

<?php include "layout.php" ?>
