<?php ob_start() ?>
<h2>Elija una opción:</h2>
<div id="subcontenedor">
    <div id="programador" class="opcion"><a href="index.php?ctl=insertarP">Programador</a></div>
    <div id="software" class="opcion"><a href="index.php?ctl=insertarS">Software</a></div>
</div>
<?php $articulo = ob_get_clean() ?>

<?php ob_start() ?>
<?php
$m = new Model();
$softwares = $m->getSoftware();
unset($m);

if (isset($softwares) && count($softwares) > 0)
{
    echo "<aside><nav><p>Software disponible:</p><ul>";
    
    foreach ($softwares as $software)
        echo '<li><a href="index.php?ctl=ver&id=' . $software->getId() . '" title="' . $software->getNombre() . '">' . $software->getNombre() . '</a></li>';
    
    echo "</ul></nav></aside>";
}
?>
<?php $menu = ob_get_clean() ?>

<?php include "layout.php" ?>
